# Documentation

## Installed UI Frameworks
- [NativeBase](https://nativebase.io/)
- [React Navigation](https://reactnavigation.org/)
- [React Native Vector Icons](https://github.com/oblador/react-native-vector-icons#android)
- [Lottie React Native](https://github.com/lottie-react-native/lottie-react-native)
- [React Redux](https://redux.js.org/introduction/getting-started)

## Image source
- [78969-money](https://lottiefiles.com/78969-money)
- [106680-login-and-sign-up](https://lottiefiles.com/106680-login-and-sign-up)

## Developer tools
- [Flipper](https://fbflipper.com/docs/features/)
- [React Native Tools](https://marketplace.visualstudio.com/items?itemName=msjsdiag.vscode-react-native) (extension VS Code)

## Missing
Additionaly for iOS platform there is a requirement to link the native parts of the library:
``` bash
    npx pod-install
    # npx pod-install ios
```
- If you're on a vanilla React Native project, you also need to install and link react-native-vector-icons

## NB
Follow **CUSTOM::** to track custom modifications in android project # cv-android
