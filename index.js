import 'react-native-gesture-handler';
import React from 'react';
import { NativeBaseProvider, extendTheme } from "native-base";
import { NavigationContainer } from '@react-navigation/native';
import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import customTheme from './src/themes';
import { PersistGate } from 'redux-persist/integration/react';

export default function Main() {
    const theme = extendTheme({ ...customTheme });

    return (
        <NavigationContainer>
            <NativeBaseProvider theme={theme}>
                        <App />
            </NativeBaseProvider>
        </NavigationContainer>
    )
}

AppRegistry.registerComponent(appName, () => Main);
