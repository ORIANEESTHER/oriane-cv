import React, { MutableRefObject } from 'react';
import { IButtonProps, Button } from 'native-base';

const ButtonCustom = (props: IButtonProps & { colorScheme: string, ref?: MutableRefObject<any>; }): JSX.Element => {
    const { ref, children, colorScheme, ...other } = props;

    return (
        <Button
            {...other}
            ref={ref}
            _text={{ fontWeight: 'bold' }}
        >
            {children}
        </Button>
    );
}

export default ButtonCustom;