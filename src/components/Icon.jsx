import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import global from '../themes/global';

const IconCustom = (props) => {
    return (
        <Icon {...props} size={props.size ?? global.fontSizes.lg} />
    )
}

export default IconCustom;