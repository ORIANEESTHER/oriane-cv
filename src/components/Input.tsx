import React from 'react';
import { IInputProps, Input, Pressable, Text, VStack, useTheme } from "native-base";

const InputComponent = (props: IInputProps & { label?: string, placeholder?: string, keyboardType?: string, hasError?: boolean }): JSX.Element => {
    const { label, placeholder, keyboardType, hasError, ...other } = props
    const { colors } = useTheme();

    return (
        <Pressable>
            <VStack space={1}>
                {
                    label && (
                        <Text
                            ml={1}
                            fontSize={'sm'}
                            color={colors.blueGray[700]}
                        >
                            {label}
                        </Text>
                    )
                }
                <Input
                    {...other}
                    variant={'unstyled'}
                    borderRadius={'xl'}
                    backgroundColor={colors.coolGray[100]}
                    color={colors.blueGray[700]}
                    placeholderTextColor={colors.blueGray[400]}
                    fontWeight={'medium'}
                    cursorColor={colors.primary[500]}
                    placeholder={placeholder}
                    keyboardType={keyboardType}
                    borderColor={hasError ? colors.danger[500] : ''}
                    borderWidth={hasError ? 1 : 0}
                />
            </VStack>
        </Pressable>
    );
}

export default InputComponent;