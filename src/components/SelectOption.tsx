import React from 'react';
import { HStack, Pressable, Text, useTheme } from "native-base";
import Icon from './Icon';


const SelectOption = (props: any & { title: string, onPress?: any }): JSX.Element => {
    const { title, onPress } = props;
    const { colors } = useTheme();

    return (
        <Pressable
            width={'100%'}
            _pressed={{
                bg: 'blueGray.100',
            }}
            onPress={onPress}
        >
            <HStack justifyContent={'space-between'} alignItems={'center'} py={4} px={6} >
                <Text justifyContent={'center'}>{title}</Text>
                <Icon color={colors.blueGray[500]} name="chevron-right" solid />
            </HStack>
        </Pressable>
    );
};

export default SelectOption;