
const navigation = {
    competences: 'Compétences',
    experiences :'Parcours professionnel',
    myProfil: 'Profil',
    bottomTabNavigator: 'BottomTabNavigator',
    splashScreen: 'SplashScreen',
    
};

export default navigation;