import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from '../components/Icon';
import Competences from '../screens/Competences';
import Experiences from '../screens/Experiences';
import navigation from '../constants/navigation';
import MyProfil from '../screens/MyProfil';

const BottomTab = createBottomTabNavigator();

const BottomTabNavigator = (): JSX.Element => {
    
    return (
        <BottomTab.Navigator>
            <BottomTab.Screen
                name={navigation.myProfil}
                component={MyProfil}
                options={{
                    tabBarIcon: ({ color }) => {
                        return <Icon color={color} name="user" solid />;


                    },
                }}
            />
            <BottomTab.Screen
                name={navigation.experiences}
                component={Experiences}
                options={{
                    tabBarIcon: ({ color }) => {
                        return <Icon color={color} name="check" solid />;
                    },
                }}
            />
            <BottomTab.Screen
                name={navigation.competences}
                component={Competences}
                options={{
                    tabBarIcon: ({ color }) => {
                        return <Icon color={color} name="book" solid />;
                    },
                }}
            />
        </BottomTab.Navigator>
    );
};

export default BottomTabNavigator;