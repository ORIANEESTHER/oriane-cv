import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import BottomTabNavigator from './BottomTabNavigator';
import navigation from '../constants/navigation';
import SplashScreen from '../screens/SplashScreen';

const Stack = createStackNavigator();

const StackNavigator = (): JSX.Element => {
    return (
        <Stack.Navigator
            initialRouteName={navigation.splashScreen}
            screenOptions={{
                headerShown: false,
            }}
        >
            <Stack.Screen
                name={navigation.splashScreen}
                component={SplashScreen}

            />
            <Stack.Screen
                name={navigation.bottomTabNavigator}
                component={BottomTabNavigator}

            />
        </Stack.Navigator>
    );
}

export default StackNavigator;