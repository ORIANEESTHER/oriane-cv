import React from 'react';
import StackNavigator from './StackNavigator';


const Navigator = (): JSX.Element => {
    return (
        <StackNavigator />
    );
};

export default Navigator;