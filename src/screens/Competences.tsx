import React from 'react';
import { Box, Heading, Text, VStack, View, useTheme } from "native-base";
import Icon from '../components/Icon';

const Competences = (props: any): JSX.Element => {
    const { colors } = useTheme();

    return (
        <Box>


            <Heading style={{ textAlign: 'center' , fontSize: 20}}>Languages</Heading>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>HTML4/CSS3</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check"/>
                <Text style={{fontSize: 17}}>JAVASCRIPT</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>REACTJS/NATIVE</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>PYTHON</Text>
            </View>


            <Heading style={{ textAlign: 'center',fontSize: 20 }}>Infrastructure</Heading>

            <View style={{ flexDirection: 'row', alignItems: 'center' , marginLeft: 20}}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>AWS COGNITO</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check" />
                <Text>AWS API GATEWAY</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>AWS LAMBDA</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>AWS S3</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' , marginLeft: 20}}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>CI/CD</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>TERRAFORM</Text>
            </View>


            <Heading style={{ textAlign: 'center',fontSize: 20 }}>Outils</Heading>


            <View style={{ flexDirection: 'row', alignItems: 'center' , marginLeft: 20}}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>Trello</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check"/>
                <Text style={{fontSize: 17}}>Replit</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}}>GIT LAB/GIT</Text>
            </View>
           

            <Heading style={{ textAlign: 'center' ,fontSize: 20}}>Bases de données</Heading>


            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check" />
                <Text style={{fontSize: 16}}>POSTGRESQL</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                <Icon name="check"  />
                <Text style={{fontSize: 16}}>MYSQL</Text>
            </View>

        </Box>

    );
};

export default Competences;