import React, { useRef } from 'react';
import Icon from '../components/Icon';
import AnimatedLottieView from 'lottie-react-native';
import { Box, Heading, Text, View, useTheme } from "native-base";

const Experiences = (props: any): JSX.Element => {
    const { colors } = useTheme();
    const animationRef = useRef<AnimatedLottieView>(null);

    return (

        <Box>
            <Heading style={{ marginLeft: 10, marginTop: 20, fontSize: 18}}>MONEY'S HOME(4 mois)</Heading>
            <Text style={{fontSize: 17,marginLeft: 10 ,fontWeight: 'bold', marginBottom : 10}} >Developpeuse Web</Text>
            
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
                <Icon name="check" />
                <Text style={{fontSize: 17}} >Assistance aux employés expérimentés.</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' , marginLeft: 10}}>
                <Icon name="check" />
                <Text style={{fontSize: 17}} >Participation aux rituels et réunions.</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}} >Contribution à l'amélioration de l'application.</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' , marginLeft: 10}}>
                <Icon name="check" />
                <Text style={{fontSize: 17}} >Assistance dans la migration vers AWS.</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}} >Aide à la finition de  l'application Android.</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' , marginLeft: 10}}>
                <Text style={{ fontWeight: 'bold',  fontSize: 16 }}>Techno: AWS, TERRAFORM, REACTJS/NATIVE, GIT</Text>
            </View>
            <Box alignItems={'center'}>
                <Box w={200} h={120}>
                <AnimatedLottieView 
                    source={require('../assets/lottie/experience.json')}
                    ref={animationRef}
                    loop={true}
                    autoPlay
                />
                </Box>
            </Box>

            <Heading style={{  marginLeft: 10,fontSize: 18}}>MIHI CONSULTING(5 mois)</Heading>
            <Text style={{fontSize: 17,marginLeft: 10 ,fontWeight: 'bold', marginBottom : 10}} >Developpeuse Fullstack</Text>
            
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}} >Développement de l'ihm utilisateur.</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                <Icon name="check"  />
                <Text style={{fontSize: 17}} >Utiliser AWS Cognito pour gérer les utilisateurs.</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}} >Utiliser AWS Lambda en Python pour le back.</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                <Icon name="check" />
                <Text style={{fontSize: 17}} >Utiliser AWS API Gateway pour les API.</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' , marginLeft: 10}}>
                <Icon name="check" />
                <Text style={{fontSize: 17}} >Utiliser AWS S3 pour le stockage des vidéos.</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' , marginLeft: 10}}>

                <Text style={{ fontWeight: 'bold' , fontSize: 16}}>Techno: AWS, REACTJS, TRELLO, PYTHON, TERRAFORM, GIT</Text>
            </View>
            
        </Box>





    );

};

export default Experiences;