import React, { useRef } from 'react';
import { Avatar, Box, Center, Heading, Text, useTheme } from "native-base";
import AnimatedLottieView from 'lottie-react-native';
const MyProfil = (props: any): JSX.Element => {
    const { colors } = useTheme();
    const animationRef = useRef<AnimatedLottieView>(null);

    return (
        <>


<Box >
    <Center>
        <Avatar bg="amber.500" source={{
            uri: "https://media.licdn.com/dms/image/D4E03AQHF5Tf5hlo6KA/profile-displayphoto-shrink_800_800/0/1674823888066?e=1701907200&v=beta&t=q9_jXfafhBxH66ldjgXPC8TkJpf4VDzly1OxqeOAHa0"
        }} size="2xl" mb={10} style={{ marginBottom : 100, marginTop: 20}}>
        </Avatar><Heading>
             Developpeuse Web/Devops
        </Heading>
        <Heading>
             ORIANE NGANGA
        </Heading>
        
       </Center>

       <Box alignItems={'center'}>
                            <Box w={300} h={300}></Box>
                            <AnimatedLottieView style={{ marginBottom : 10, marginTop: 30}}
                                source={require('../assets/lottie/accueil-cv.json')}
                                ref={animationRef}
                                loop={true}
                                autoPlay
                            />
                        </Box>
</Box>



        </>
    );
};

export default MyProfil;