import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import { Box, Center, HStack, PresenceTransition, Text, useTheme } from "native-base";
import constantNavigation from '../constants/navigation';

const SplashScreen = (props: any): JSX.Element => {
    const { navigation } = props;
    const { colors } = useTheme();

    const goNextStep = async () => {
        navigation.navigate(constantNavigation.bottomTabNavigator);
    }

    useEffect(() => {
        const timeout = setTimeout(() => {
            goNextStep();
        }, 1000);

        return () => clearTimeout(timeout);
    }, [])

    return (
        <>
            <StatusBar
                backgroundColor={colors.primary[50]}
                barStyle={'dark-content'}
            />
            <Box bgColor={colors.primary[50]} flex={1} justifyContent={'center'} alignItems={'center'}>
                <Box>
                    <PresenceTransition visible={true} initial={{
                        opacity: 0,
                        scale: 0
                    }} animate={{
                        opacity: 1,
                        scale: 1,
                        transition: {
                            duration: 250
                        }
                    }}>
                        <Center>
                            <HStack space={3} top={-150}>
                                <Text fontSize={'4xl'} fontWeight={'semibold'} color={colors.black}>ORIANE NGANGA</Text>
                            </HStack>
                        </Center>
                    </PresenceTransition>
                </Box>
            </Box >
        </>
    );
};

export default SplashScreen;