import { InterfaceButtonProps } from "native-base/lib/typescript/components/primitives/Button/types";

const baseStyle = (props: InterfaceButtonProps & { theme: any }) => {
    return {
        borderRadius: 'xl',
    };
};


function variantOutline({ colorScheme }: InterfaceButtonProps) {
    return {
        borderWidth: '2px',
        borderColor: 'muted.300',
        _text: {
            color: `${colorScheme}.900`,
        },
        _icon: {
            color: `${colorScheme}.500`,
        },
        _spinner: {
            color: `${colorScheme}.500`,
        },
        _hover: {
            bg: `${colorScheme}.500:alpha.10`,
        },
        _pressed: {
            bg: `${colorScheme}.500:alpha.20`,
        },
    };
}

const button = {
    baseStyle,
    defaultProps: {
        size: 'lg'
    },
    variants: {
        outline: variantOutline,
    },
};

export default button;