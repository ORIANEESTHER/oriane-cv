import colors from "./colors";
import global from "./global";
import button from "./button";
import text from "./text";

const theme = {
    config: {
        initialColorMode: 'light',
    },
    colors,
    components: {
        Text: text,
        Button: button,
        Input: {
            defaultProps: {
                size: 'lg',
                variant: 'rounded',
            }
        },
        Heading: {
            baseStyle: {
                color: global.textColor
            },
        },
    },
};

export default theme;