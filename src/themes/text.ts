import global from "./global";

const getSizes = () => {
    const sizes = {} as any;
    Object.keys(global.fontSizes).forEach(x => sizes[x] = { fontSize: `${global.fontSizes[x]}px` });

    return sizes;
};

const text = {
    baseStyle: {
        color: global.textColor
    },
    defaultProps: {
        size: 'lg'
    },
    sizes: getSizes(),
}

export default text;